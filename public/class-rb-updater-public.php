<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Rb_Updater
 * @subpackage Rb_Updater/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Rb_Updater
 * @subpackage Rb_Updater/public
 * @author     Your Name <email@example.com>
 */
class Rb_Updater_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $rb_updater    The ID of this plugin.
	 */
	private $rb_updater;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $rb_updater       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $rb_updater, $version ) {

		$this->rb_updater = $rb_updater;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rb_Updater_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rb_Updater_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->rb_updater, plugin_dir_url( __FILE__ ) . 'css/rb-updater-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Rb_Updater_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Rb_Updater_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->rb_updater, plugin_dir_url( __FILE__ ) . 'js/rb-updater-public.js', array( 'jquery' ), $this->version, false );

	}

}
